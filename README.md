git fetch --all
git reset --hard origin/master

# flutter_routes_exercise1

In this project I will be testing the navigation and the different routes and see how they behave.

### How to make route not repush when its the same route?

A way is to create a global variable and save the current page.

Another way is to use this:

```dart
    ModalRoute.of(context).settings.name);
```

But this only works if you're on a named route. When your drawer is open and you're using this piece of code,
it will not work because your drawer is not a named route and the return value will be null.

### Overlay or OverlayRoute

As far as I can tell, every Navigator has an Overlay widget which is kind of like a Stack widget meant presumably for screens.

We can access the current Overlay by

```dart
    Overlay.of(context)
```

This Overlay widget is an independent widget.

One thing I noticed is that you cant directly insert an overlay in the build function or initstate.
It will cause a redraw before the widgets are even built. It has to happen by a button press or something else. When using a overlay widget as screen, you can't pop it, as it's not a route. So you can't get data from it aswell.

The Navigator uses this Overlay widget aswell, aswell as having its own Overlay widget. When we insert something in the Overlay, we insert something at the top, but the navigator has its own overlays, below that. Meaning when we navigate to a screen from the Overlay, the screen doesn't get pushed on top of the Overlay. Below a simple picture.

OverlayWidget(

------------------------ OverlayScreen

------------------------ Navigator(OverlayWidget(

-------------------- ScreenpushedByOverlay

-------------------- HomeScreen
))
)
This is what I assume is happening.

##### OverlayRoute

```dart
  Iterable<OverlayEntry> createOverlayEntries() {
    // TODO: implement createOverlayEntries
    return [
      OverlayEntry(
        builder: (context) {
          return Center(
            child: Container(
              width: 200,
              height: 200,
              color: Colors.blue,
            ),
          );
        },
      ),
    ];
  }
```

A class extended by OverlayRoute only needs to override the createOverlayEntries. In this method you can pass your widgets to the overlay as an entry to show up on screen.

An OverlayRoute can be called with Navigator.push as it takes in a type of Route which OverlayRoute extends.
We can navigate to it with Navigator.push() and our OverlayRoute gets inserted in the Overlay in the Navigator. Afterwards when we pop it, this route will get popped because its in fact a route. We can interact with the screen below it because its not a modalroute.

Our OverlayRoute, however, has no animation. That's what the MaterialPageRoute is for, it extends ModalRoute which extends TransitionRoute so it can have native transitions.

Note:
Interesting note is when using a drawer with a homescreen and then pushing the OverlayRouteScreen over the homescreen, after using the drawer from the homescreen, the drawer gets pushed below the OverlayRouteScreen. My guess is its using the context of the HomeScreen to insert itself.

### TransitionRoute

So in order to make animations when our screen shows, we can use the TransitionRoute. This route extends the OverlayRoute, meaning that we can still communicate with the underlying route if it's visible.

There are 3 methods that has to be overridden.
The createOverlayEntries method, which is the same as the Overlay one.

```dart
    @override
    bool get opaque => true;

    @override
    Duration get transitionDuration => Duration(seconds: 2);
```

Opaque means whether the previous route will be obscured when the transition completes. The routes behind will not be built, so the background will be black.

The transition of this route will end after the transitionDuration. The background will be black, after the entrance transition ends en before the end transition starts.

Can I change the color of this background? Well, it doesn't really matter. You can always put a background in the createOverlayEntries. I guess that's why the background will enter after transition and exit before transition out. So that it will not be noticable by the user.

The TransitionRoute exposes animation and controller for you to use. 
```dart
AnimatedBuilder(
    animation: animation,
    builder: (context, child) {
    return Center(
        child: Container(
        width: animation.value * 300,
    ...
);
```

You can override the createAnimation method to provide your own animation like below. This animation (which is the same as the above one) triggers when a route gets pushed or popped(animates in reverse).

```dart
  @override
  Animation<double> createAnimation() {
    Animation<double> _controller = super.createAnimation();
    return CurvedAnimation(parent: _controller, curve: Curves.easeIn);
  }
```

Can I change the animation when my route is popped instead of reversing the entrance animation? Yes there is!
The animation has a status field which you can check whether the animation is going forward, backwards or is completed and a few more statuses! So if you want to implement something hacky. Use the following value to increase your screen in size when popping.

```dart
double value =
(animation.status != AnimationStatus.reverse)
? animation.value * 300
: 300 + ((animation.value * -1) + 1) * 300;
```

There might be a better way but I'm not going to look further into this.

In the TransitionRoute there's a secondary animation, which will trigger when a route gets pushed on top or leaves. This is, I guess, useful for creating a kind of slide animation that trigger when a route gets pushed on top. Is it very efficient? Probably not, but when you set your route as opaque, the underlying screen will not get built, so it might be okay? Not sure but I will not do a performance test until I need it.

Same stuff for the secondary animation. The primary animation exposes the animation property, the secondary animation exposes the secondaryAnimation property for you to use.

### ModalRoute

ModalRoute is an inherited child of a TransitionRoute. The difference here is that ModalRoute can have a layer covering the entire Navigator aka layer below and lets you not interact with the route below. That layer doesn't necessarily has to be opaque like the TransitionRoute.

```dart
  @override
  // TODO: implement barrierDismissible
  bool get barrierDismissible => null;

  @override
  // TODO: implement barrierLabel
  String get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    // TODO: implement buildPage
    return null;
  }

  @override
  Color get barrierColor => null;

  @override
  // TODO: implement maintainState
  bool get maintainState => null;

  @override
  // TODO: implement opaque
  bool get opaque => null;

  @override
  // TODO: implement transitionDuration
  Duration get transitionDuration => null;
```

If barrierDismissible is true, the route can be popped by tapping the barrier.
barrierColor variable is ofcourse for the color of the barrier.

So yea, there are a lot of Routes that inherit from ModalRoute like PopupRoute, DialogRoute, PageRoute, so if there's something you need to know. You could study those routes.

The route is pretty easy to understand, but what is interesting is, is that ModalRoute has a mixin called LocalHistoryRoute. This Mixin lets you pop an internal screen from a route. Kind of like a presentation where you have a slide with one thing popping after another. With LocalHistoryRoute you're able to go back to previous 'thing' in the slide.

```dart
void _buttonPress() {
  setState(() => counter++);
  ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: () {
    // Hide the red rectangle.
    setState(() => counter--);
  }));
}
```

I made a simple counter app to show the inner workings. The addLocalHistoryEntry makes sure that theres an entry to remove when pressing the back button or android's backbutton. Pretty neat. Kind of like nested navigation, but very limited.
