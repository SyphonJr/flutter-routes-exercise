import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/constants.dart';

import '../main.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          _drawerHeader(),
          _drawerItem(
            title: 'Home',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, homeRoute);
            },
          ),
          _drawerItem(
            title: 'Second',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, secondRoute);
            },
          ),
          _drawerItem(
            title: 'Overlay',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, overlayRoute);
            },
          ),
          _drawerItem(
            title: 'OverlayRoute',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, overlayRouteRoute);
            },
          ),
          _drawerItem(
            title: 'OverlayRoute2',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, overlayRouteRoute2);
            },
          ),
          _drawerItem(
            title: 'TransitionRoute',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, transitionRoute);
            },
          ),
          _drawerItem(
            title: 'modalRoute',
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, modalRoute);
            },
          ),
        ],
      ),
    );
  }
}

Widget _drawerHeader() {
  return DrawerHeader(
    child: Text('Routepages and navigation'),
  );
}

Widget _drawerItem({String title, GestureTapCallback onTap}) {
  return ListTile(
    title: Text(title),
    onTap: onTap,
  );
}
