import 'package:flutter/material.dart';

import '../constants.dart';

class OverlayTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 200,
        height: 200,
        child: ListView(
          children: <Widget>[
            Text('Overlay'),
            RaisedButton(
              child: Text('Navigate to second'),
              onPressed: () {
                Navigator.pushReplacementNamed(context, secondRoute);
              },
            ),
            RaisedButton(
              child: Text('Pop'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
