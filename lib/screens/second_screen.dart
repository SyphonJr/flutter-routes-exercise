import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/screens/overlayroute_screen.dart';

import '../widgets/app_drawer.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text('Second screen'),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(
            child: Text('Navigate'),
            onPressed: () {
              print('secondscreen nav');
              Navigator.push(context, OverlayRouteScreen());
            },
          ),
          RaisedButton(
            child: Text('Pop'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          Center(
            child: Text('Second Screen'),
          ),
        ],
      ),
    );
  }
}
