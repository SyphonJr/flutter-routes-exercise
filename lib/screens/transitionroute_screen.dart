import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/constants.dart';
import 'package:flutter_routes_exercise1/widgets/app_drawer.dart';

class TransitionRouteScreen extends TransitionRoute {
  @override
  Iterable<OverlayEntry> createOverlayEntries() {
    // TODO: implement createOverlayEntries
    return [
      OverlayEntry(
        builder: (context) {
          return AnimatedBuilder(
              animation: animation,
              builder: (context, child) {
                double value = (animation.status != AnimationStatus.reverse)
                    ? animation.value * 300
                    : 300 + ((animation.value * -1) + 1) * 300;
                return Center(
                  child: Container(
                    width: value,
                    height: 300,
                    color: Colors.green,
                    child: Column(
                      children: <Widget>[
                        RaisedButton(
                          child: Text('Navigate to home'),
                          onPressed: () {
                            Navigator.pushReplacementNamed(context, homeRoute);
                          },
                        ),
                        RaisedButton(
                          child: Text('Pop'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        Center(
                          child: Text('Transition Screen'),
                        ),
                      ],
                    ),
                  ),
                );
              });
        },
      ),
    ];
  }

  @override
  Animation<double> createAnimation() {
    Animation<double> _controller = super.createAnimation();
    return CurvedAnimation(parent: _controller, curve: Curves.easeIn);
  }

  @override
  bool get opaque => true;

  @override
  Duration get transitionDuration => Duration(seconds: 2);
}
