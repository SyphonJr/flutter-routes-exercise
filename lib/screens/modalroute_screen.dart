import 'package:flutter/material.dart';

class ModalRouteScreen extends ModalRoute {
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return Scaffold(
      appBar: AppBar(title: Text('Hello')),
      body: Center(
        child: Counter(),
      ),
    );
  }

  @override
  // TODO: implement barrierColor
  Color get barrierColor => Colors.grey;

  @override
  // TODO: implement barrierDismissible
  bool get barrierDismissible => true;

  @override
  // TODO: implement barrierLabel
  String get barrierLabel => 'label';

  @override
  // TODO: implement maintainState
  bool get maintainState => true;

  @override
  // TODO: implement opaque
  bool get opaque => false;

  @override
  // TODO: implement transitionDuration
  Duration get transitionDuration => Duration(milliseconds: 200);
}

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int counter = 0;

  void _buttonPress() {
    setState(() => counter++);
    ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: () {
      // Hide the red rectangle.
      setState(() => counter--);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        child: Text(counter.toString()),
        onPressed: () {
          _buttonPress();
        },
      ),
    );
  }
}
