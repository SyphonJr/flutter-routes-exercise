import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/constants.dart';
import 'package:flutter_routes_exercise1/widgets/app_drawer.dart';

import '../main.dart';
import 'package:flutter_routes_exercise1/constants.dart';

class OverlayRouteScreen extends OverlayRoute {
  @override
  Iterable<OverlayEntry> createOverlayEntries() {
    // TODO: implement createOverlayEntries
    return [
      OverlayEntry(
        builder: (context) {
          return Center(
            child: Container(
                width: 200,
                height: 200,
                color: Colors.blue,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('pop'),
                )),
          );
        },
      ),
    ];
  }
}

class OverlayRouteScreen2 extends OverlayRoute {
  @override
  Iterable<OverlayEntry> createOverlayEntries() {
    // TODO: implement createOverlayEntries
    return [
      OverlayEntry(
        builder: (context) {
          return Scaffold(
            drawer: AppDrawer(),
            appBar: AppBar(
              title: Text('OverlayRoute2'),
            ),
            body: Column(
              children: <Widget>[
                RaisedButton(
                  child: Text('Navigate to home'),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, homeRoute);
                  },
                ),
                RaisedButton(
                  child: Text('Pop'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Center(
                  child: Text('Overlay2 Screen'),
                ),
              ],
            ),
          );
        },
      ),
    ];
  }
}
