import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/widgets/overlay_test.dart';

import '../main.dart';
import '../widgets/app_drawer.dart';

class OverlayScreen extends StatefulWidget {
  @override
  _OverlayScreenState createState() => _OverlayScreenState();
}

class _OverlayScreenState extends State<OverlayScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _overlay = Overlay.of(context);

    return Scaffold(
      body: Column(
        children: <Widget>[
          Center(
            child: Text('Overlay Screen'),
          ),
          RaisedButton(
            onPressed: () {
              _overlay.insert(
                OverlayEntry(
                  builder: (context) {
                    return OverlayTest();
                  },
                ),
              );
            },
            child: Text('Create overlay'),
          ),
        ],
      ),
      appBar: AppBar(),
      drawer: AppDrawer(),
    );
  }
}
