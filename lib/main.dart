import 'package:flutter/material.dart';
import 'package:flutter_routes_exercise1/constants.dart';
import 'package:flutter_routes_exercise1/screens/modalroute_screen.dart';
import 'package:flutter_routes_exercise1/screens/overlay_screen.dart';
import 'package:flutter_routes_exercise1/screens/overlayroute_screen.dart';
import 'package:flutter_routes_exercise1/screens/second_screen.dart';
import 'package:flutter_routes_exercise1/screens/transitionroute_screen.dart';
import 'package:flutter_routes_exercise1/widgets/app_drawer.dart';

import 'constants.dart';
import 'constants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case homeRoute:
            return MaterialPageRoute(builder: (context) => HomeScreen());
          case secondRoute:
            return MaterialPageRoute(builder: (context) => SecondScreen());
          case overlayRoute:
            return MaterialPageRoute(builder: (context) => OverlayScreen());
          case overlayRouteRoute:
            return OverlayRouteScreen();
          case overlayRouteRoute2:
            return OverlayRouteScreen2();
          case transitionRoute:
            return TransitionRouteScreen();
          case modalRoute:
            return ModalRouteScreen();
        }
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: Text('Home screen'),
      ),
      drawer: AppDrawer(),
    );
  }
}
